<?php
if ($this->car_list != null) {
	if ($this->mobile_detect->isMobile()) {
	
		//---------------------------------------------
		// variables for website design

		$carouselHeight = 300; //px
		$carouselWidth = 677; //px
		$carouselRows = 1;
		$carouselColumns = 2;
		$carouselImgMargin = 5;
		$carouselIntervalSpeed = 3000; //ms  length between each animation
		$carouselTransitionSpeed = 1500; //ms  animation speed
		
		//---------------------------------------------
		
		if ($this->mobile_detect->isTablet()) {
			//---------------------------------------------
			// variables for website design
		$carouselHeight = 200; //px
		$carouselWidth = 700; //px
		$carouselRows = 1;
		$carouselColumns = 3;
		$carouselImgMargin = 5;
		$carouselIntervalSpeed = 3000; //ms  length between each animation
		$carouselTransitionSpeed = 1500; //ms  animation speed
			//---------------------------------------------
		}
		
	} 
	
	else {
		$template = $this->settings['CarouselTemplate'];
		$css = $this->settings['CarouselTemplateCSS'];
		//---------------------------------------------
		// variables for website design

		$carouselHeight = 230; //px
		$carouselWidth = 900; //px
		$carouselRows = 1;
		$carouselColumns = 4;
		$carouselImgMargin = 5;
		$carouselIntervalSpeed = 100000; //ms  length between each animation
		$carouselTransitionSpeed = 1500; //ms  animation speed

		//---------------------------------------------
	}

$carouselImgHeight = (int) ((($carouselHeight - 65) - ($carouselRows * ($carouselImgMargin * 2))) / $carouselRows);
$carouselImgWidth = (int) (($carouselWidth - ($carouselColumns * ($carouselImgMargin * 2))) / $carouselColumns);
$carouselCarsToDisplay = [];
$year = array();
$make = array();
$model = array();
$carouselCarsToDisplayCount;
$carouselSlideCount;
$carouselMasterSlideWidth;
$carouselSlideWidth = $carouselColumns * ($carouselImgWidth + ($carouselImgMargin * 2));

//build list of displayable cars

foreach($this->car_list as $car)
{
    if ($car->ImageUrls[0])
	{
		$year[] = $car->Year;
		$make[] = $car->Make;
		$model[] = $car->Model;
		$carouselCarsToDisplay[] = $car;
		
	}
}
shuffle($carouselCarsToDisplay);
$carouselCarsToDisplayCount = count($carouselCarsToDisplay);

//detemermine number of slides
if($carouselCarsToDisplayCount % ($carouselRows * $carouselColumns) > 0)
{
    $carouselSlideCount = (int) ($carouselCarsToDisplayCount / ($carouselRows * $carouselColumns)) + 1;
}else
{
    $carouselSlideCount = (int) $carouselCarsToDisplayCount / ($carouselRows * $carouselColumns);
}
$carouselSlideCount++; //for duplicate slide

//determine master slide width
$carouselMasterSlideWidth = $carouselSlideCount * (($carouselImgWidth * $carouselColumns) + ($carouselImgMargin * 2));
$carouselMasterSlideWidth += ($carouselMasterSlideWidth * 0.2);

//construct slides

    //-------------
    //$carouselWidth = 5000;
    //-------------
}
?>
<!--<div id='carouselContainer' style='height:<?php echo $carouselHeight; ?>px; width:<?php echo $carouselWidth; ?>px; overflow: hidden; margin: 0 auto;'>-->

<div id='carouselContainer' style='height:<?php echo $carouselHeight; ?>px; width:<?php echo $carouselWidth; ?>px; overflow: hidden; margin: 0 auto;'>
    <div id='carouselMasterSlide' style='width: <?php echo $carouselMasterSlideWidth; ?>px; height: 100%; left: 0px; position: relative;'>
        <?php
		
        for($i = 0; $i < $carouselSlideCount-1; $i++)
        {
            ?>
            <div id='<?php echo 'carouselSlide'.$i; ?>' style='height: 100%; width: <?php echo $carouselSlideWidth; ?>px; float: left;'>
            <?php
 
            for($j = $i * ($carouselRows * $carouselColumns); $j < ($i * ($carouselRows * $carouselColumns)) + ($carouselRows * $carouselColumns); $j++) //this loop iterates through the index(s) of $carouselCarsToDisplay for the slide number specified by $i
            { 
                //if $j is not too big
                if($j < $carouselCarsToDisplayCount)
                {
                    ?>
                    <!--<a href='<?php echo $this->generate_detail_url($carouselCarsToDisplay[$j]); ?>' style='height:auto; width: auto; float: left; box-shadow:1px 1px 1px 1px gray; margin: <?php echo $carouselImgMargin; ?>px; display: block; position: relative;'>-->
                    <a href='<?php echo $this->generate_detail_url($carouselCarsToDisplay[$j]); ?>' style='width: auto; float: left; box-shadow:0px 2px 3px 1.5px gray;  margin: <?php echo $carouselImgMargin; ?>px; display: block; position: relative; padding: 5px; background: white;'>
					
					
	
					
					
			
					
					
					
					
                        
						<!--<img src='<?php echo $carouselCarsToDisplay[$j]->ImageUrls[0]; ?>' style='height:auto; width: auto;' />-->
						
						<!--Year-Make-Model-->
								
								<h6 style='height: 10px; vertical-align: bottom; letter-spacing: -0.5px; font-weight: 550;'>
										<?php echo $carouselCarsToDisplay[$j]->Year;?>&nbsp;
										<?php echo $carouselCarsToDisplay[$j]->Make;?>&nbsp;
										<?php echo $carouselCarsToDisplay[$j]->Model;?>
								</h6>
								
						<!--Year-Make-Model-->
						
						
						<div style='border-radius: 2px; height:<?php echo $carouselImgHeight; ?>px; overflow: hidden; position: relative;'>
							

								
								
								
								<!--Pricing--->
								<div style='position: absolute; bottom: 0; right: -10px; width: <?php if ($carouselCarsToDisplay[$j]->Price == 0){ echo '85%';}	else { echo '50%'; }?>; height: 50px; transform: skew(-14deg, 0deg); background: red;'></div>
								
							
							
							
								<h4 style='position: absolute; bottom: -10px; right: 5px; color: white; font-weight: bold;'><?php if ($carouselCarsToDisplay[$j]->Price == 0)
								{ 
									echo ' Call for Pricing'; 
								}
								else {
									echo '$'; 
									echo $carouselCarsToDisplay[$j]->Price; 
								}	
								?></h4>
								<!--Pricing--->
							
							
							<img src='<?php echo $carouselCarsToDisplay[$j]->ImageUrls[0]; ?>' style='height:<?php echo $carouselImgHeight; ?>px; width: <?php echo $carouselImgWidth; ?>px;' />
						</div>
						
													
							
						
						
                    </a>  
                    <?php
                }
            }
            ?>
            </div>
            <?php
        }
            
            //if there is more than one slide, output the first slide again as the last slide
            if($carouselCarsToDisplayCount > ($carouselRows * $carouselColumns))
            {
                for($i = 0; $i < 1; $i++) //only loop through once
                {
                    ?>
                    <div id='<?php echo 'carouselSlideDuplicate'; ?>' style='width: <?php echo $carouselSlideWidth; ?>px; display: inline-block; overflow: hidden;'>
                    <?php

                    for($j = $i * ($carouselRows * $carouselColumns); $j < ($i * ($carouselRows * $carouselColumns)) + ($carouselRows * $carouselColumns); $j++) //this loop iterates through the index(s) of $carouselCarsToDisplay for the slide number specified by $i
                    { 
                        //if $j is not too big
                        if($j < $carouselCarsToDisplayCount)
                        {
                            ?>
                             <a href='<?php echo $this->generate_detail_url($carouselCarsToDisplay[$j]); ?>' style='height: <?php echo $carouselImgHeight; ?>px; width: <?php echo $carouselImgWidth; ?>px; float: left; margin: <?php echo $carouselImgMargin; ?>px;'><p style="font-size:13px; text-align:center; font-weight: bolder;"> $<?php  echo $carouselCarsToDisplay[$j]->Price; ?></p>
                        <img src='<?php echo $carouselCarsToDisplay[$j]->ImageUrls[0]; ?>' style='height:<?php echo $carouselImgHeight; ?>px; width: <?php echo $carouselImgWidth; ?>px;' /><p style="font-size:12px; text-align:center; font-weight: bolder;"><?php echo $carouselCarsToDisplay[$j]->Year;?><?php echo $carouselCarsToDisplay[$j]->Make;?> </p>
                    </a>  
                            <?php
                        }
                    }
                    ?>
                    </div>
                    <?php
                }
            }
        ?>
    </div>
</div>

<script>
    
    var carouselCurrentSlide = 0;
    var carouselSlideCount = <?php echo json_encode($carouselSlideCount); ?>;
    var carouselSlideWidth = <?php echo json_encode($carouselSlideWidth); ?>;
    var carouselIntervalSpeed = <?php echo json_encode($carouselIntervalSpeed); ?>;
    var carouselTransitionSpeed = <?php echo json_encode($carouselTransitionSpeed); ?>;
    
    if(carouselSlideCount > 2)
    {
        setInterval(function()
        {
            if((carouselCurrentSlide + 2) == carouselSlideCount)
            {
                jQuery('#carouselMasterSlide').animate({left: '-='+carouselSlideWidth}, carouselTransitionSpeed);
                jQuery('#carouselMasterSlide').animate({left: '0px'}, 0);
                carouselCurrentSlide = 0;
            }else
            {
                jQuery('#carouselMasterSlide').animate({left: '-='+carouselSlideWidth}, carouselTransitionSpeed);
                carouselCurrentSlide++;
            }
        },carouselIntervalSpeed);
    }
    
</script>