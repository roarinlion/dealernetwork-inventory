<?php

$terms_list = array();
$display_list = array();

foreach ($this->car_list as $car) {
	array_push($terms_list, $car->Category);
	array_push($terms_list, $car->Year);
	array_push($terms_list, $car->Make);
	array_push($terms_list, $car->Model);
}

foreach($terms_list as $term)
{
	if (!in_array($term, $display_list)) array_push($display_list, $term);
}
?>

<div id="dn-inv-search">
<?php
$val = isset($_GET[$this->filter_keys['search']]) ? $_GET[$this->filter_keys['search']] : '';
?>
<form method="get" id="inventorysearch" action="<?php echo $this->current_url; ?>">
	<label class="screen-reader-text" for="q">Search for:</label>
	<input type="text" name="q" id="q" value="<?php echo $val; ?>" />
	<?php foreach ($_GET as $key=>$value) { if ($key == $this->filter_keys['search']) continue; ?>
	<input type="hidden" name="<?php echo $key; ?>" id="<?php echo $key; ?>" value="<?php echo $value; ?>" />
	<?php } ?>
	<input type="submit" value="SEARCH">
</form>
</div>

<script>
  jQuery(function()
  {
    var availableTags =
	[
  <?php
	$terms_count = count($display_list);
	for ($i = 0; $i < $terms_count - 2; $i++)
	{
		echo '"'.$display_list[$i].'",';
		echo "\n";
	}
	echo '"'.$display_list[$terms_count - 1].'"';
	?>
    ];
	
    jQuery( "#q" ).autocomplete({
      source: availableTags
    });
  });
 </script>